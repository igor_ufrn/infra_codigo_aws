import sys
import boto3
import traceback
from printDictInPrettyJson import print_dict_to_pretty_json
from datetime import date


def quotas():
    try:
        print("INICIO DA GRAVACAO DAS COTAS...")
        # Salvando referência para saída padrão
        original_stdout = sys.stdout
        totalQuotas=0
        with open('quotas.txt', 'w') as f:
            sys.stdout = f

            client = boto3.client('service-quotas')
            next_token = None
            response = None
            while True:
                if next_token:
                    response = client.list_services(NextToken=next_token, MaxResults=100)
                else:
                    response = client.list_services(MaxResults=100)

                services = response['Services']

                for service in services:
                    print(service)

                    quotas_by_service = client.list_service_quotas(
                        ServiceCode=service['ServiceCode'],
                        MaxResults=100
                    )

                    print('--------------QUOTAS DESTE SERVICO------------------')
                    print("Total = {}".format(len(quotas_by_service['Quotas'])))
                    print_dict_to_pretty_json(quotas_by_service)
                    print('--------------FIM QUOTAS DESTE SERVICO------------------')

                    quotas_default_by_service = client.list_aws_default_service_quotas(
                        ServiceCode=service['ServiceCode'],
                        MaxResults=100
                    )

                    print('--------------QUOTAS DEFAULT DESTE SERVICO------------------')
                    print("Total = {}".format(len(quotas_default_by_service['Quotas'])))
                    print_dict_to_pretty_json(quotas_default_by_service)
                    print('--------------FIM QUOTAS DEFAULT DESTE SERVICO------------------')

                    totalQuotas = totalQuotas + len(quotas_default_by_service['Quotas'])

                if 'NextToken' in response:
                    next_token = response['NextToken']
                else:
                    break
        sys.stdout = original_stdout
        print("FIM DA GRAVACAO DAS COTAS")
        print("TOTAL DE COTAS DEFAULT = {}".format(totalQuotas))


    except BaseException as error:
        print('Ocorreu um erro inesperado no quotas: {}'.format(error))
        traceback.print_exc()


def lambda_handler(event, context):
    quotas()


if __name__ == "__main__":
    quotas()
