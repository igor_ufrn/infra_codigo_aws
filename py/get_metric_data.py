
import boto3
import traceback
from printDictInPrettyJson import print_dict_to_pretty_json

from datetime import datetime
from datetime import timezone
from datetime import timedelta


def get_metric_data():
    try:
        client = boto3.client('cloudwatch')

        response = client.get_metric_data(
            MetricDataQueries=[
                {
                    'Id': 'usage_data',
                    'MetricStat': {
                        'Metric': {
                            'Namespace': 'AWS/Usage',
                            'MetricName': 'ResourceCount',
                            'Dimensions': [
                                {
                                    'Name': 'Class',
                                    'Value': 'Standard/OnDemand'
                                },
                                {
                                    'Name': 'Resource',
                                    'Value': 'vCPU'
                                },
                                {
                                    'Name': 'Service',
                                    'Value': 'EC2'
                                },
                                {
                                    'Name': 'Type',
                                    'Value': 'Resource'
                                }
                            ]
                        },
                        'Period': 1,
                        'Stat': 'Maximum',
                        'Unit': 'None'
                    },
                    'Label': 'Quantidade de máquinas EC2, independentemente do Estado',
                    'ReturnData': True
                },
            ],
            StartTime=datetime.now(timezone.utc) - timedelta(minutes=5),
            EndTime=datetime.now(timezone.utc)
            #NextToken='string',
            #MaxDatapoints=1

        )

        # response = client.list_metrics(
        #     Namespace='AWS/Usage',
        #     MetricName='ResourceCount',
        #     Dimensions=[
        #         {
        #             'Name': 'Class',
        #             'Value': 'Standard/OnDemand'
        #         },
        #         {
        #             'Name': 'Resource',
        #             'Value': 'vCPU'
        #         },
        #         {
        #             'Name': 'Service',
        #             'Value': 'EC2'
        #         },
        #         {
        #             'Name': 'Type',
        #             'Value': 'Resource'
        #         }
        #     ]
        # )

        print(response)
        print(datetime.now(timezone.utc) - timedelta(minutes=5))
        print(datetime.now(timezone.utc))
        #print(datetime.datetime(2021, 10, 16))
        #print(datetime.datetime(2021, 10, 19))

    except BaseException as error:
        print('Ocorreu um erro inesperado no get_metric_data: {}'.format(error))
        traceback.print_exc()


def lambda_handler(event, context):
    get_metric_data()


if __name__ == "__main__":
    get_metric_data()
