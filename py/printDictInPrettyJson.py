import traceback
from datetime import datetime
from datetime import date
import json


def default(o):
    if isinstance(o, (date, datetime)):
        return o.isoformat()


def print_dict_to_pretty_json(dicionario):
    try:
        json_object = json.dumps(
            dicionario,
            sort_keys=True,
            indent=1,
            default=default
        )
        print(json_object)

    except BaseException as error:
        print('Ocorreu um erro inesperado: {}'.format(error))
        traceback.print_exc()


if __name__ == "__main__":
    print_dict_to_pretty_json({'time': datetime.now()})
