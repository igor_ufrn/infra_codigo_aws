import boto3
import traceback
from datetime import datetime
from datetime import timezone


def put_custom_metric_total_db_instances():
    try:
        print('Iniciando função put_metric_total_db_instances')
        client = boto3.client('rds')
        response = client.describe_db_instances()
        total_db_instances = len(response['DBInstances'])
        response = client.describe_reserved_db_instances()
        total_reserved_db_instances = len(response['ReservedDBInstances'])
        print("total_db_instances = {}".format(total_db_instances))
        print("total_reserved_db_instances = {}".format(total_reserved_db_instances))

        client = boto3.client('cloudwatch')
        response = client.put_metric_data(
            Namespace='Custom/RDS',
            MetricData=[
                {
                    'MetricName': 'CountDBInstances',
                    'Timestamp': datetime.now(timezone.utc),
                    'Value': total_db_instances,
                    'Unit': 'Count'
                },
            ]
        )
        print(response)

        response = client.put_metric_data(
            Namespace='Custom/RDS',
            MetricData=[
                {
                    'MetricName': 'CountReservedDBInstances',
                    'Timestamp': datetime.now(timezone.utc),
                    'Value': total_reserved_db_instances,
                    'Unit': 'Count'
                },
            ]
        )
        print(response)

    except BaseException as error:
        print('Ocorreu um erro inesperado no start_rds: {}'.format(error))
        traceback.print_exc()


def lambda_handler(event, context):
    put_custom_metric_total_db_instances()


if __name__ == "__main__":
    put_custom_metric_total_db_instances()
