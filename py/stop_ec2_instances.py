import boto3
import traceback
from datetime import datetime


def stop_ec2_instances():
    try:
        print('Parando as instâncias com tag ScheduledStartStop=True')
        ec2_client = boto3.client('ec2', region_name='us-east-1')
        instances = ec2_client.describe_instances()
        instance_ids = list()

        for reservation in instances['Reservations']:
            for instance in reservation['Instances']:
                if instance['State']['Name'] == "running" and not instance['Tags'] is None:
                    for tag in instance['Tags']:
                        if tag['Key'] == 'ScheduledStartStop' and tag['Value'] == 'True':
                            instance_ids.append(instance['InstanceId'])

        if len(instance_ids) > 0:
            print("Parando instâncias: " + str(instance_ids))
            ec2_client.stop_instances(InstanceIds=instance_ids, Force=False)

    except BaseException as error:
        print('Ocorreu um erro inesperado no stop_ec2_instances: {}'.format(error))
        traceback.print_exc()


def lambda_handler(event, context):
    stop_ec2_instances()


if __name__ == "__main__":
    stop_ec2_instances()
