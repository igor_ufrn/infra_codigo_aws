import boto3
import traceback


def start_rds():
    try:
        print('Iniciando função que habilita RDS')
        client = boto3.client('rds')
        response = client.describe_db_clusters()
        total_clusters_modificados=0
        for db_cluster in response['DBClusters']:
            if db_cluster['Status'] == 'stopped':
                print('Iniciando o Cluster RDS:')
                print(db_cluster)
                resultado = client.start_db_cluster(DBClusterIdentifier=db_cluster['DBClusterIdentifier'])
                print("Dados da resposta")
                print(resultado)
                total_clusters_modificados=total_clusters_modificados+1

        print("Total de Clusters iniciados = {}".format(total_clusters_modificados))

    except BaseException as error:
        print('Ocorreu um erro inesperado no start_rds: {}'.format(error))
        traceback.print_exc()


def lambda_handler(event, context):
    start_rds()


if __name__ == "__main__":
    start_rds()
