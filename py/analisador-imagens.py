import boto3
from printDictInPrettyJson import print_dict_to_pretty_json
import json


# detecta faces
def analisar():
    client = boto3.client('rekognition')
    # A função index_faces() identifica as faces em uma imagem de origem e adiciona o resultado em uma coleção.
    faces_detectadas = client.index_faces(
        CollectionId='personagens',
        DetectionAttributes=['DEFAULT'],
        ExternalImageId='TEMPORARIA',
        Image={
            'S3Object': {
                'Bucket': 'igorlinnik-imagens',
                'Name': 'tokio_berlin_nairobi.png'
            }
        }

    )
    return faces_detectadas


def cria_lista_face_id_detectadas(faces_detectadas):
    face_id_detectadas = []
    for i in range(len(faces_detectadas['FaceRecords'])):
        face_id_detectadas.append(faces_detectadas['FaceRecords'][i]['Face']['FaceId'])
    return face_id_detectadas


def compara_imagens(face_id_detectadas):
    client = boto3.client('rekognition')
    resultado_comparacao = []
    for id in face_id_detectadas:
        resultado_comparacao.append(
            client.search_faces(
                CollectionId='personagens',
                FaceId=id,
                MaxFaces=10,
                FaceMatchThreshold=70
            )

        )
    return resultado_comparacao


def gera_dados_json(resultado_comparacao):
    dados_json = []
    for face_match in resultado_comparacao:
        if (len(face_match.get('FaceMatches'))) >= 1:
            perfil = dict(nome=face_match['FaceMatches'][0]['Face']['ExternalImageId'],
                          faceMatch=round(face_match['FaceMatches'][0]['Similarity'], 2))
            dados_json.append(perfil)
    return dados_json

def publica_dados(dados_json):
    s3 = boto3.resource('s3')
    arquivo = s3.Object('igorlinnik-imagens-site', 'dados.json')
    arquivo.put(Body=json.dumps(dados_json))

def exclui_imagens_colecao(face_id_detectadas):
    client = boto3.client('rekognition')
    client.delete_faces(CollectionId='personagens', FaceIds=face_id_detectadas)


if __name__ == "__main__":
    faces_detectadas = analisar()
    face_id_detectadas = cria_lista_face_id_detectadas(faces_detectadas)
    resultado_comparacao = compara_imagens(face_id_detectadas)
    dados_json = gera_dados_json(resultado_comparacao)
    publica_dados(dados_json)
    exclui_imagens_colecao(face_id_detectadas)
    print_dict_to_pretty_json(dados_json)

# aws s3 sync . s3://igorlinnik-imagens
# aws rekognition list-collections
# aws rekognition list-faces --collection-id personagens
# aws lambda update-function-code --function-name funcao --zip-file fileb://arquivo.zip
# aws lambda publish-version --function-name funcao
# aws lambda create-alias --function-name funcao --function-version 1  --name PROD