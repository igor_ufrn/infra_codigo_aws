import boto3
import traceback


def start_ec2_instances():
    try:
        print('Iniciando as instâncias com tag ScheduledStartStop=True')
        ec2_client = boto3.client('ec2', region_name='us-east-1')
        instances = ec2_client.describe_instances()
        instance_ids = list()

        for reservation in instances['Reservations']:
            for instance in reservation['Instances']:
                if instance['State']['Name'] == "stopped" and not instance['Tags'] is None:
                    for tag in instance['Tags']:
                        if tag['Key'] == 'ScheduledStartStop' and tag['Value'] == 'True':
                            instance_ids.append(instance['InstanceId'])

        if len(instance_ids) > 0:
            print("Iniciando instâncias: " + str(instance_ids))
            ec2_client.start_instances(InstanceIds=instance_ids)

    except BaseException as error:
        print('Ocorreu um erro inesperado: {}'.format(error))
        traceback.print_exc()


def lambda_handler(event, context):
    start_ec2_instances()


if __name__ == "__main__":
    start_ec2_instances()
