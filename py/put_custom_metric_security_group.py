import boto3
import traceback

from datetime import datetime
from datetime import timezone


def put_metric_data():
    try:
        put_custom_metric_security_group()
    except BaseException as error:
        print('Ocorreu um erro inesperado no put_metric_data: {}'.format(error))
        traceback.print_exc()


def put_custom_metric_security_group():
    client = boto3.client('ec2')

    next_token = None

    securityGroups = []
    while True:
        if next_token:
            response = client.describe_security_groups(NextToken=next_token)
        else:
            response = client.describe_security_groups()

        securityGroups.extend(response['SecurityGroups'])

        if 'NextToken' in response:
            next_token = response['NextToken']
        else:
            break

    for sg in securityGroups:
        print("GroupId = {}".format(sg['GroupId']))
        print("Regras de entrada")
        print(sg['IpPermissions'])
        print("Regras de saida")
        print(sg['IpPermissionsEgress'])
        entrada = 0
        for i in sg['IpPermissions']:
            entrada = entrada + len(i['IpRanges']) + len(i['Ipv6Ranges']) + len(i['PrefixListIds']) + len(
                i['UserIdGroupPairs'])

        saida = 0
        for i in sg['IpPermissionsEgress']:
            saida = saida + len(i['IpRanges']) + len(i['Ipv6Ranges']) + len(i['PrefixListIds']) + len(
                i['UserIdGroupPairs'])

        total_regras = (entrada + saida)
        print("Entrada = {}".format(entrada))
        print("Saida = {}".format(saida))
        print("Total = {}".format(total_regras))
        client = boto3.client('cloudwatch')
        response = client.put_metric_data(
            Namespace='Custom/SecurityGroup',
            MetricData=[
                {
                    'MetricName': 'CountRulesSecurityGroup',
                    'Dimensions': [
                        {
                            'Name': 'GroupId',
                            'Value': sg['GroupId']
                        }
                    ],
                    'Timestamp': datetime.now(timezone.utc),
                    'Value': total_regras,
                    'Unit': 'Count'
                },
            ]
        )


def lambda_handler(event, context):
    put_metric_data()


if __name__ == "__main__":
    put_metric_data()
