import boto3
import traceback


def stop_rds():
    try:
        print('Iniciando função que desabilita RDS')
        client = boto3.client('rds')
        response = client.describe_db_clusters()
        total_clusters_modificados = 0
        for db_cluster in response['DBClusters']:
            if db_cluster['Status'] == 'available':
                print('Parando o Cluster RDS:')
                print(db_cluster)
                resultado = client.stop_db_cluster(DBClusterIdentifier=db_cluster['DBClusterIdentifier'])
                print("Dados da resposta")
                print(resultado)
                total_clusters_modificados = total_clusters_modificados + 1

        print("Total de Clusters parados = {}".format(total_clusters_modificados))

    except BaseException as error:
        print('Ocorreu um erro inesperado no stop_rds: {}'.format(error))
        traceback.print_exc()


def lambda_handler(event, context):
    stop_rds()


if __name__ == "__main__":
    stop_rds()
