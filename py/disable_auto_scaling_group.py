import boto3
import traceback


def disable_auto_scaling_group():
    try:
        print('Iniciando função que desabilita auto_scaling_groups')
        client = boto3.client('autoscaling')
        response = client.describe_auto_scaling_groups()
        for auto_scaling_group in response['AutoScalingGroups']:
            print('Alterando propriedades do seguinte auto_scaling_group:')
            print(auto_scaling_group)
            min_size = 0
            max_size = 0
            desired_capacity = 0
            print('Propriedade MinSize: de {} para {}:'.format(auto_scaling_group['MinSize'], min_size))
            print('Propriedade MaxSize: de {} para {}:'.format(auto_scaling_group['MaxSize'], max_size))
            print('Propriedade DesiredCapacity: de {} para {}:'.format(auto_scaling_group['DesiredCapacity'], desired_capacity))

            resultado = client.update_auto_scaling_group(
                AutoScalingGroupName=auto_scaling_group['AutoScalingGroupName'],
                MinSize=min_size,
                MaxSize=max_size,
                DesiredCapacity=desired_capacity
            )
            print("Dados da resposta")
            print(resultado)

    except BaseException as error:
        print('Ocorreu um erro inesperado no disable_auto_scaling_group: {}'.format(error))
        traceback.print_exc()


def lambda_handler(event, context):
    disable_auto_scaling_group()


if __name__ == "__main__":
    disable_auto_scaling_group()
