import boto3
import traceback


def stop_emr():
    try:
        print('Iniciando função que desabilita EMR')
        client = boto3.client('emr')
        client_ec2 = boto3.client('ec2')
        list_clusters = client.list_clusters()
        for cluster in list_clusters['Clusters']:
            print("----------Cluster----------")
            print(cluster)
            print("----------Cluster----------")
            if cluster['Status']['State'] == 'WAITING':
                print(client.get_managed_scaling_policy(ClusterId=cluster['Id']))
                print(client.list_instance_groups(ClusterId=cluster['Id']))

                response = client.modify_instance_groups(
                    ClusterId=cluster['Id'],
                    InstanceGroups=[{'InstanceGroupId': 'ig-164ACVGDON30E', 'InstanceCount': 2}]
                )

                response = client.put_managed_scaling_policy(
                    ClusterId=cluster['Id'],
                    ManagedScalingPolicy={
                        'ComputeLimits': {
                            'UnitType': 'Instances',
                            'MinimumCapacityUnits': 1,
                            'MaximumCapacityUnits': 2,
                            'MaximumOnDemandCapacityUnits': 1,
                            'MaximumCoreCapacityUnits': 1
                        }
                    }
                )

                list_instances = client.list_instances(ClusterId=cluster['Id'], InstanceGroupTypes=['CORE'])
                for instance in list_instances['Instances']:
                    print(instance)
                    """
                    if instance['Status']['State'] == 'RUNNING':
                        client_ec2.stop_instances(InstanceIds=[instance['Ec2InstanceId']], Force=False)
                        break
                    """

    except BaseException as error:
        print('Ocorreu um erro inesperado no stop_emr: {}'.format(error))
        traceback.print_exc()


def lambda_handler(event, context):
    stop_emr()


if __name__ == "__main__":
    stop_emr()

"""
ssh -i AcessoInstanciasEC2_Key.pem hadoop@ec2-3-219-216-125.compute-1.amazonaws.com
hive
create database if not exists testdb;
use testdb;
create external table if not exists employee (nome string);
insert into table employee values('Igor Linnik');
select * from employee;
create table if not exists usuario (nome string);
insert into table usuario values('Igor Linnik');
select * from usuario;
"""
