import boto3


def lista_imagens():
    s3 = boto3.resource('s3')
    bucket = s3.Bucket('igorlinnik-imagens')
    imagens = []
    for imagem in bucket.objects.all():
        imagens.append(imagem.key)
    return imagens


def indexar(imagens):
    client = boto3.client('rekognition')
    # client.delete_collection(CollectionId='personagens')
    client.create_collection(CollectionId='personagens')

    for imagem in imagens:
        response = client.index_faces(
            CollectionId='personagens',
            ExternalImageId=imagem[:-4],
            Image={
                'S3Object': {
                    'Bucket': 'igorlinnik-imagens',
                    'Name': imagem
                }
            }

        )


if __name__ == "__main__":
    imagens = lista_imagens()
    indexar(imagens)

# aws s3 sync . s3://igorlinnik-imagens
# aws rekognition list-collections
# aws rekognition list-faces --collection-id personagens
