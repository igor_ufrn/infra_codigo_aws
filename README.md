Este demonstra, através de diversos exemplos, uso de libs da AWS para 
automatizar tarefas na nuvem e economizar recursos:

- automatização de start de máquinas EC2
- automatização de stop de máquinas EC2
- automatização de start do serviço AWS RDS
- automatização de stop do serviço AWS RDS
- Inserção de métricas customizadas em serviços AWS
- Automatização da desativação do Cluster EKS com objetivo de reduzir custos
- Automatização da ativação do Cluster EKS a partir de determinado horário
- Definição de cotas de serviço AWS
